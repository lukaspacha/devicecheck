//
//  DeviceCheck.m
//  docutool
//
//  Created by Lukas Pacha on 14.11.11.
//  Copyright (c) 2011 Lukas Pacha iPhone Development. All rights reserved.
//

#import "DeviceCheck.h"
#include <sys/sysctl.h>

@implementation DeviceCheck

+ (BOOL) isiPad2{
    
    return [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera];
}

+ (BOOL)isRetina
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) {
        return YES;
    }
    return NO;
}

+ (BOOL)isiPadMini{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size + 1);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    machine[size] = 0;
    
    if (strcmp(machine, "iPad2,7") == 0) {
        return YES;
    }
    
    free(machine);
    
    return NO;
}

@end
