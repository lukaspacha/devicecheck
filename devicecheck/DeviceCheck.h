//
//  DeviceCheck.h
//  docutool
//
//  Created by Lukas Pacha on 14.11.11.
//  Copyright (c) 2011 Lukas Pacha iPhone Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceCheck : NSObject
+ (BOOL) isiPad2;
+ (BOOL) isRetina;
+ (BOOL)isiPadMini;
@end
