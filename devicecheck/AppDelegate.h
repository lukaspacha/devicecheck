//
//  AppDelegate.h
//  devicecheck
//
//  Created by Lukas Pacha on 25.01.13.
//  Copyright (c) 2013 Lukas Pacha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
